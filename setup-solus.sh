#!/bin/bash
if [[ $EUID -ne 0 ]] || [ -z ${SUDO_USER+x} ]; then
   echo "This script must be run as sudo under your main user" 1>&2
   exit 1
fi

cd

eopkg up -y

snap install spotify
eopkg it -c system.devel -y
eopkg it xrdb zsh git git-lfs telegram htop etcher golang zip unzip neofetch aria2 acpi ncmpcpp vim epson-inkjet-printer-escpr flatpak texlive-all solbuild pip discord lm_sensors cava rxvt-unicode -y

sudo -u $SUDO_USER git lfs install

eopkg lr | grep solbuilds -q
if [[ $? -ne 1 ]]; then
    eopkg rr solbuilds
fi

eopkg ar solus.sh https://repo.solus.sh/eopkg-index.xml.xz
eopkg er solus.sh
eopkg it chrome dotnet-sdk geogebra insync jetbrains-toolbox libspotify libspotify-devel sublime-text-dev xarchiver cli-visualizer -y

sudo eopkg bi --ignore-safety https://raw.githubusercontent.com/solus-project/3rd-party/master/programming/tools/android-tools/pspec.xml
sudo eopkg it android-tools*.eopkg;sudo rm android-tools*.eopkg

if [[ ! -f "/usr/bin/vis-real" ]]; then
    mv /usr/bin/vis /usr/bin/vis-real
    echo '#!/bin/bash' > /usr/bin/vis
    echo 'xrdb -load ~/.Xresources' >> /usr/bin/vis
    echo 'urxvt -e bash ~/.vis.sh &' >> /usr/bin/vis
    chmod +x /usr/bin/vis
fi

solbuild update
if [[ $? == 1 ]]; then
	solbuild init
	solbuild update
fi

pip install mopidy mopidy-spotify mopidy-spotify-web mopidy-iris

sudo -u $SUDO_USER flatpak install --user --from https://download.mono-project.com/repo/monodevelop.flatpakref -y

cp /home/$SUDO_USER/.local/share/flatpak/exports/share/applications/*.desktop /home/$SUDO_USER/.local/share/applications

if [[ ! -f /home/$SUDO_USER/".dotfiles_installed" ]]; then
    wget https://solus.sh/dotfiles.zip -O /tmp/dotfiles.zip
    unzip -o /tmp/dotfiles.zip -d /home/$SUDO_USER/
    touch /home/$SUDO_USER/".dotfiles_installed"
fi

sudo -u $SUDO_USER bash -c "$(curl -fsSL solus.sh/solus-ohmyz.sh)"

cat /home/$SUDO_USER/.zshrc | grep transfer.sh -q
if [[ $? -ne 0 ]]; then
    curl -fsSL https://gist.githubusercontent.com/anonymous/c2a9e0ea86b5fb9c7971182f461e33e4/raw/567a15b8491cb9106c53835c731e583d3b12d7b5/gistfile1.txt >> /home/$SUDO_USER/.zshrc
fi

chown $SUDO_USER:$SUDO_USER /home/$SUDO_USER -R

cat /home/$SUDO_USER/.zshrc | grep "alias gst=" -q
if [[ $? -ne 0 ]]; then
    printf "\n" >> /home/$SUDO_USER/.zshrc
    echo "alias gst='git status'" >> /home/$SUDO_USER/.zshrc
    echo "alias gad='git add'" >> /home/$SUDO_USER/.zshrc
    echo "alias gcm='git commit'" >> /home/$SUDO_USER/.zshrc
    echo "alias gcmm='git commit -m'" >> /home/$SUDO_USER/.zshrc
    echo "alias gps='git push'" >> /home/$SUDO_USER/.zshrc
    echo "alias gpl='git pull'" >> /home/$SUDO_USER/.zshrc
    echo "alias grm='git rm'" >> /home/$SUDO_USER/.zshrc
fi
