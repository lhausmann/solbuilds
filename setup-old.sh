#!/bin/bash
cd

sudo eopkg up -y

sudo snap install spotify
sudo eopkg it -c system.devel -y
sudo eopkg it xrdb zsh git git-lfs telegram htop etcher golang zip unzip cava neofetch aria2 acpi ncmpcpp rxvt-unicode vim epson-inkjet-printer-escpr flatpak texlive-all solbuild pip discord -y

git lfs install

eopkg lr | grep solbuilds -q
if [[ $? -ne 1 ]]; then
    sudo eopkg rr solbuilds
fi

sudo eopkg ar solus.sh https://repo.solus.sh/eopkg-index.xml.xz
sudo eopkg er solus.sh
sudo eopkg it chrome cli-visualizer dotnet-sdk geogebra insync jetbrains-toolbox libspotify libspotify-devel sublime-text-dev xarchiver -y

if [[ ! -f "/usr/bin/vis-real" ]]; then
    sudo mv /usr/bin/vis /usr/bin/vis-real
    sudo bash -c "echo '#!/bin/bash' > /usr/bin/vis"
    sudo bash -c "echo 'xrdb -load ~/.Xresources' > /usr/bin/vis"
    sudo bash -c "echo 'bash -c "urxvt -e ~/.vis.sh" &' > /usr/bin/vis"
    echo '#!/bin/bash' > ~/.vis.sh
    echo 'export TERM=rxvt-256color' > ~/.vis.sh
    echo 'echo -e "\e]2;\a"' > ~/.vis.sh
    echo 'vis-real' > ~/.vis.sh
fi

sudo solbuild update
if [[ $? == 1 ]]; then
	sudo solbuild init
	sudo solbuild update
fi

sudo pip install mopidy mopidy-spotify mopidy-spotify-web mopidy-iris

flatpak install --user --from https://download.mono-project.com/repo/monodevelop.flatpakref -y

if [[ ! -f ~/".dotfiles_installed" ]]; then
    wget https://solus.sh/dotfiles.zip -O /tmp/dotfiles.zip
    unzip -o /tmp/dotfiles.zip -d .
    touch ~/".dotfiles_installed"
fi

bash -c "$(curl -fsSL solus.sh/solus-ohmyz.sh)"

cat .zshrc | grep transfer.sh -q
if [[ $? -ne 0 ]]; then
    curl -fsSL https://gist.githubusercontent.com/anonymous/c2a9e0ea86b5fb9c7971182f461e33e4/raw/567a15b8491cb9106c53835c731e583d3b12d7b5/gistfile1.txt >> .zshrc
fi

cat .zshrc | grep "alias gst=" -q
if [[ $? -ne 0 ]]; then
    printf "\n" >> .zshrc
    echo "alias gst='git status'" >> .zshrc
    echo "alias gad='git add'" >> .zshrc
    echo "alias gcm='git commit'" >> .zshrc
    echo "alias gcmm='git commit -m'" >> .zshrc
    echo "alias gps='git push'" >> .zshrc
    echo "alias gpl='git pull'" >> .zshrc
    echo "alias grm='git rm'" >> .zshrc
fi
